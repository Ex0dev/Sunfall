#include "stdafx.h"
#include "string.h"

size_t HashedString::hash_name(const std::string& IdentStr)
{
	static const size_t InitialFNV = 2166136261U;
	static const size_t FNVMultiple = 16777619;

	size_t Hash = InitialFNV;
	for (size_t i = 0; i < IdentStr.length(); i++)
	{
		Hash ^= IdentStr[i];
		Hash *= FNVMultiple;
	}
	return Hash;
}