#pragma once

template <class BaseType, class SubType>
BaseType* GenericObjectCreationFunction(void) { return new SubType; }

template <class BaseClass, class IdType>
class GenericObjectFactory
{
	typedef BaseClass* (*ObjectCreationFunction)(void);
	std::map<IdType, ObjectCreationFunction> m_CreationFunctions;

public:
	template <class SubClass>
	bool Register(IdType Id)
	{
		auto Iter = m_CreationFunctions.find(Id);
		if (Iter == m_CreationFunctions.end())
		{
			m_CreationFunctions[Id] = &GenericObjectCreationFunction<BaseClass, SubClass>;
			return true;
		}
		return false;
	}

	BaseClass* Create(IdType Id)
	{
		auto Iter = m_CreationFunctions.find(Id);
		if (Iter != m_CreationFunctions.end())
		{
			ObjectCreationFunction Func = Iter->second;
			return Func();
		}
		return NULL;
	}
};