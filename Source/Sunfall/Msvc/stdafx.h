// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

#define WIN32_LEAN_AND_MEAN             // Exclude rarely-used stuff from Windows headers
// Windows Header Files:
#include <windows.h>

// C RunTime Header Files
#include <stdlib.h>
#include <malloc.h>
#include <memory.h>
#include <tchar.h>
#include <stdio.h>

#include <memory>
using std::shared_ptr;
using std::weak_ptr;
using std::static_pointer_cast;
using std::dynamic_pointer_cast;

#include <string>
#include <algorithm>
#include <string>
#include <list>
#include <vector>
#include <queue>
#include <map>
#include <fstream>

// TODO: reference additional headers your program requires here
#include <irrlicht.h>
using namespace irr;
using namespace core;
using namespace scene;
using namespace video;
using namespace io;
using namespace gui;

#include <json.hpp>
using nlohmann::json;

#include "..\Utilities\templates.h"
#include "..\Utilities\interfaces.h"

int main(int argc, char** argv);
INT WINAPI SunFallMain(HINSTANCE Instance,
	HINSTANCE PrevInstance,
	LPTSTR CommandLine,
	INT ShowCode);