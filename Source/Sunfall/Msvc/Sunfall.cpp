#include "stdafx.h"

#include "..\Core\Game.h"

#ifdef _IRR_WINDOWS_
#pragma comment(lib, "irrlicht.lib")

#if defined(_DEBUG) | defined(NDEBUG)
#pragma comment(linker, "/subsystem:console /ENTRY:mainCRTStartup")
#else
#pragma comment(linker, "/subsystem:windows /ENTRY:WinMainCRTStartup")
#endif
#endif

#if defined(_DEBUG) | defined(NDEBUG)
int main(int argc, char** argv)
{
	UNREFERENCED_PARAMETER(argc);
	UNREFERENCED_PARAMETER(argv);

	SunFallMain((HINSTANCE)GetModuleHandle(NULL), 0, 0, SW_SHOW);

	return 0;
}
#endif

INT WINAPI SunFallMain(HINSTANCE Instance,
	HINSTANCE PrevInstance,
	LPTSTR CommandLine,
	INT ShowCode)
{
	UNREFERENCED_PARAMETER(Instance);
	UNREFERENCED_PARAMETER(PrevInstance);
	UNREFERENCED_PARAMETER(CommandLine);
	UNREFERENCED_PARAMETER(ShowCode);

	Game& GameInstance = Game::GetGame();

	if (!GameInstance.Initialize(1280, 720, _T("Sunfall"))) return 1;
	GameInstance.RunLoop();
	if (!GameInstance.DestroyObjects()) return 1;

	return 0;
}