#include "stdafx.h"
#include "ActorFactory.h"

#include <fstream>

#include "Actor.h"
#include "ActorComponent.h"
#include "TransformComponent.h"
#include "RenderComponent.h"

ActorFactory::ActorFactory(void)
{
	m_LastActorId = INVALID_ACTOR_ID;

	m_ComponentFactory.Register<TransformComponent>(ActorComponent::GetIdFromName(TransformComponent::Name));
	m_ComponentFactory.Register<MeshRenderComponent>(ActorComponent::GetIdFromName(MeshRenderComponent::Name));
}

StrongActorPtr ActorFactory::CreateActor(std::string ActorResource, const ActorId ServersActorId)
{
	std::ifstream Resource(ActorResource);
	json Root = json::parse(Resource);
	
	ActorId NextActorId = ServersActorId;
	if (NextActorId == INVALID_ACTOR_ID)
		NextActorId = GetNextActorId();

	StrongActorPtr NewActor(new Actor(NextActorId));
	if (!NewActor->Init(Root))
	{
		return StrongActorPtr();
	}

	auto Components = Root["components"];
	for (auto Iter = Components.begin(); Iter != Components.end(); Iter++)
	{
		auto CompName = Iter.key();
		auto CompJson = Iter.value();
		StrongActorComponentPtr NewComponent(VCreateComponent(CompName.c_str(), CompJson));
		if (NewComponent)
		{
			NewActor->AddComponent(NewComponent);
			NewComponent->SetOwner(NewActor);
		}
		else
		{
			return StrongActorPtr();
		}
	}

	NewActor->PostInit();

	return NewActor;
}

StrongActorComponentPtr ActorFactory::VCreateComponent(const std::string& Name, json& Root)
{
	StrongActorComponentPtr NewComponent(m_ComponentFactory.Create(ActorComponent::GetIdFromName(Name)));

	if (NewComponent)
	{
		if (!NewComponent->VInit(Root))
		{
			return StrongActorComponentPtr();
		}
	}
	else
	{
		return StrongActorComponentPtr();
	}

	return NewComponent;
}