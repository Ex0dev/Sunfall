#pragma once

typedef std::string ActorType;

class Actor
{
	friend class ActorFactory;
	typedef std::map<ComponentId, StrongActorComponentPtr> ActorComponents;

private:
	ActorId m_Id;
	ActorComponents m_Components;
	ActorType m_Type;

	std::string m_Resource;

public:
	explicit Actor(ActorId Id);
	~Actor(void);

	bool Init(json& Data);
	void PostInit(void);
	void Destroy(void);
	void Update(f32 Delta);

	//std::string ToJSON();

	ActorId GetId(void) const { return m_Id; }
	ActorType GetType(void) const { return m_Type; }

	template <class ComponentType>
	weak_ptr<ComponentType> GetComponent(size_t ComponentId)
	{
		auto Iter = m_Components.find(ComponentId);
		if (Iter != m_Components.end())
		{
			StrongActorComponentPtr Base(Iter->second);
			shared_ptr<ComponentType> Sub(static_pointer_cast<ComponentType>(Base));
			weak_ptr<ComponentType> WeakSub(Sub);
			return WeakSub;
		}
		else
		{
			return weak_ptr<ComponentType>();
		}
	}

	template <class ComponentType>
	weak_ptr<ComponentType> AddComponent(const std::string& Name)
	{
		ComponentId Id = ActorComponent::GetIdFromName(Name);
		auto Iter = m_Components.find(Id);
		if (Iter != m_Components.end())
		{
			StrongActorComponentPtr Base(Iter->second);
			shared_ptr<ComponentType> Sub(static_pointer_cast<ComponentType>(Base));
			weak_ptr<ComponentType> WeakSub(Sub);
			return WeakSub;
		}
		else
		{
			return weak_ptr<ComponentType>();
		}
	}

	const ActorComponents* GetComponents() { return &m_Components; }

	void AddComponent(StrongActorComponentPtr Component);
};