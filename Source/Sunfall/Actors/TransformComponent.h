#pragma once

#include "ActorComponent.h"

class TransformComponent : public ActorComponent
{
	matrix4 m_Transform;

public:
	static std::string Name;
	virtual const std::string& VGetName() const { return Name; }

	TransformComponent(void) {}
	virtual bool VInit(json& Data) override;

	matrix4 GetTransform(void) const { return m_Transform; }
	void SetPosition(vector3df NewPosition) { m_Transform.setTranslation(NewPosition); }
	vector3df GetPosition(void) const { return m_Transform.getTranslation(); }
	void SetRotation(vector3df NewRotation) { m_Transform.setRotationDegrees(NewRotation); }
	vector3df GetRotation(void) const { return m_Transform.getRotationDegrees(); }
};