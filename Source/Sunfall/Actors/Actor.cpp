#include "stdafx.h"
#include "Actor.h"

#include "ActorComponent.h"

Actor::Actor(ActorId Id)
{
	m_Id = Id;
	m_Type = "Unknown";

	m_Resource = "Unknown";
}

Actor::~Actor(void)
{
	assert(m_Components.empty());
}

bool Actor::Init(json& Data)
{
	m_Type = Data["type"].get<std::string>();
	m_Resource = Data["resource"].get<std::string>();
	return true;
}

void Actor::PostInit(void)
{
	for (auto Iter = m_Components.begin(); Iter != m_Components.end(); Iter++)
	{
		Iter->second->VPostInit();
	}
}

void Actor::Destroy(void)
{
	m_Components.clear();
}

void Actor::Update(f32 Delta)
{
	for (auto Iter = m_Components.begin(); Iter != m_Components.end(); Iter++)
	{
		Iter->second->VUpdate(Delta);
	}
}

void Actor::AddComponent(StrongActorComponentPtr Component)
{
	m_Components.insert(std::make_pair(Component->VGetId(), Component));
}