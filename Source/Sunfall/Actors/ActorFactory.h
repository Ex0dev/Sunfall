#pragma once

class ActorFactory
{
	ActorId m_LastActorId;

protected:
	GenericObjectFactory<ActorComponent, ComponentId> m_ComponentFactory;

public:
	ActorFactory(void);

	StrongActorPtr CreateActor(std::string ActorResource, const ActorId ServersActorId);
	
	virtual StrongActorComponentPtr VCreateComponent(const std::string& Name, json& Data);

private:
	ActorId GetNextActorId(void) { ++m_LastActorId; return m_LastActorId; }
};