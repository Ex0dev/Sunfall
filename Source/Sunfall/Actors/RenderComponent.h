#pragma once

#include "ActorComponent.h"

typedef shared_ptr<ISceneNode> StrongSceneNodePtr;

class BaseRenderComponent : public ActorComponent
{
protected:
	SColor m_Color;
	std::string m_Name;
	StrongSceneNodePtr m_SceneNode;

public:
	virtual bool VInit(json& data) override;
	virtual void VPostInit(void) override;
	const SColor GetColor(void) const;

protected:
	virtual StrongSceneNodePtr VCreateSceneNode(void) = 0;
	SColor LoadColor(json& Data);

private:
	virtual StrongSceneNodePtr VGetSceneNode(void);
};

class MeshRenderComponent : public BaseRenderComponent
{
protected:
	shared_ptr<IMesh> m_Mesh;

public:
	static std::string Name;
	virtual const std::string& VGetName(void) const { return Name; }

	virtual bool VInit(json& Data) override;

protected:
	virtual StrongSceneNodePtr VCreateSceneNode();
};