#include "stdafx.h"
#include "TransformComponent.h"

std::string TransformComponent::Name = "TransformComponent";
bool TransformComponent::VInit(json& Data)
{
	json PositionData = Data["position"];
	json RotationData = Data["rotation"];

	vector3df Position{ PositionData["x"].get<float>(), PositionData["y"].get<float>(), PositionData["z"].get<float>() };
	vector3df Rotation{ RotationData["yaw"].get<float>(), RotationData["pitch"].get<float>(), RotationData["roll"].get<float>() };

	SetPosition(Position);
	SetRotation(Rotation);

	return true;
}