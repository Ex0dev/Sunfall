#include "stdafx.h"
#include "..\Core\Game.h"
#include "RenderComponent.h"

std::string MeshRenderComponent::Name = "MeshRenderComponent";
/*
 * Render Component
 */
bool BaseRenderComponent::VInit(json& Data)
{
	json ColorData = Data["color"];
	if (!ColorData.is_null())
		m_Color = LoadColor(ColorData);
	m_Name = Data["name"].get<std::string>();
	return true;
}

void BaseRenderComponent::VPostInit(void)
{
	m_SceneNode = VGetSceneNode();
	if (!m_SceneNode)
		return;
	printf("Name: %s\n", m_SceneNode->getName());
}

SColor BaseRenderComponent::LoadColor(json& Data)
{
	SColor Color;
	Color.setRed(Data["red"].get<u32>());
	Color.setGreen(Data["green"].get<u32>());
	Color.setBlue(Data["blue"].get<u32>());
	Color.setAlpha(Data["alpha"].get<u32>());
	return Color;
}

StrongSceneNodePtr BaseRenderComponent::VGetSceneNode(void)
{
	if (!m_SceneNode)
		return VCreateSceneNode();
	return m_SceneNode;
}

/*
 * MeshRenderComponent
 */
bool MeshRenderComponent::VInit(json& Data)
{
	if (!BaseRenderComponent::VInit(Data))
		return false;

	ISceneManager* SceneManager = Game::GetGame().GetDevice()->getSceneManager();
	shared_ptr<IMesh> Mesh(SceneManager->getMesh(Data["model"].get<std::string>().c_str()));
	if (!Mesh)
		return false;
	m_Mesh = Mesh;
	return true;
}

StrongSceneNodePtr MeshRenderComponent::VCreateSceneNode(void)
{
	Game& Instance = Game::GetGame();
	StrongSceneNodePtr Node;
	if (Instance.WasSceneLoaded())
	{
		Node = std::make_shared<ISceneNode>(Instance.GetDevice()->getSceneManager()->getSceneNodeFromName(m_Name.c_str()));
	}
	else
	{
		Node = std::make_shared<ISceneNode>(Instance.GetDevice()->getSceneManager()->addMeshSceneNode(m_Mesh.get()));
		Node->setName(m_Name.c_str());
		Node->setID(HashedString::hash_name(m_Name));
	}
	return Node;
}