#pragma once

#include "..\Utilities\string.h"

class ActorComponent
{
	friend class ActorFactory;

protected:
	StrongActorPtr m_Owner;

public:
	virtual ~ActorComponent(void) { m_Owner.reset(); }

	virtual bool VInit(json& Data) = 0;
	virtual void VPostInit(void) {}
	virtual void VUpdate(f32 Delta) {}

	virtual ComponentId VGetId(void) const { return GetIdFromName(VGetName()); }
	virtual const std::string& VGetName() const = 0;
	static ComponentId GetIdFromName(const std::string& Name)
	{
		return static_cast<ComponentId>(HashedString::hash_name(Name));
	}

private:
	void SetOwner(StrongActorPtr NewOwner) { m_Owner = NewOwner; }
};