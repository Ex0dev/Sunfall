#pragma once

#include "EventReceiver.h"
#include "..\Actors\ActorFactory.h"

class Game
{
public:
	enum GameState
	{
		Invalid = 0,
		Initializing,
		Running,
		ShuttingDown,
		ShutDown
	};

	Game() { m_State = Initializing; }
	~Game() {}

	bool Initialize(int Width, int Height, LPCTSTR Title);
	bool InitObjects();
	bool DestroyObjects();

	void RunLoop();
	void Update(f32 DeltaTime);
	void Render(f32 DeltaTime);

	IrrlichtDevice* GetDevice(void) const { return m_Device; }

	static Game& GetGame(void)
	{
		static Game* instance = new Game();
		return *instance;
	}

private:
	// Irrlicht Stuff
	IrrlichtDevice* m_Device;
	IVideoDriver* m_Driver;
	ISceneManager* m_SceneManager;
	IGUIEnvironment* m_GUIEnv;

	EventReceiver* m_Receiver;

	// Options
	f32 m_MovementSpeed = 5.0f;
	GameState m_State;
	std::string m_CurrentScene;
	std::string m_SceneFile;
	bool m_SceneLoaded;

	// Actors
	ActorFactory m_ActorFactory;
	StrongActorPtr m_Astronaut;
	IMesh* m_AstronautMesh;
	IMeshSceneNode* m_AstronautSceneNode;

public:
	GameState GetGameState() const { return m_State; }
	bool WasSceneLoaded() const { return m_SceneLoaded; }
};
