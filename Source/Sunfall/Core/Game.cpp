#include "stdafx.h"

//#include "..\Actors\ActorFactory.h"
#include "..\Actors\Actor.h"
#include "Game.h"


bool Game::Initialize(int Width, int Height, LPCTSTR Title)
{
	m_Device = createDevice(video::EDT_SOFTWARE, dimension2d<u32>(Width, Height), 16);
	if (!m_Device) return false;
	m_Device->setWindowCaption(Title);

	m_Driver = m_Device->getVideoDriver();
	m_SceneManager = m_Device->getSceneManager();
	m_GUIEnv = m_Device->getGUIEnvironment();
	
#if !defined(_DEBUG) | !defined(NDEBUG)
	m_Device->getFileSystem()->addFileArchive("Assets.zip", true, true, EFAT_ZIP);
#endif

	m_SceneLoaded = m_SceneManager->loadScene("scene0.irr");
	m_CurrentScene = "scene0";
	std::stringstream SStream;
	SStream << "Scenes/" << m_CurrentScene << ".irr";
	SStream >> m_SceneFile;

	if (!InitObjects()) return false;

	m_GUIEnv->addStaticText(_T("Software Renderer"), rect<s32>(10, 10, 260, 22), true);

	//SceneManager->addCameraSceneNode(0, vector3df(0, 30, -40), vector3df(0, 5, 10));
	/*SKeyMap KeyMap[4];
	KeyMap[0].Action = EKA_MOVE_FORWARD;
	KeyMap[0].KeyCode = KEY_KEY_W;

	KeyMap[1].Action = EKA_MOVE_BACKWARD;
	KeyMap[0].KeyCode = KEY_KEY_S;

	KeyMap[2].Action = EKA_STRAFE_LEFT;
	KeyMap[2].KeyCode = KEY_KEY_A;

	KeyMap[3].Action = EKA_STRAFE_RIGHT;
	KeyMap[3].KeyCode = KEY_KEY_D;

	m_SceneManager->addCameraSceneNodeFPS(NULL, 100, 20, -1, KeyMap, 4);*/
	m_SceneManager->addCameraSceneNodeFPS(NULL, 100, 0.02);
	m_Device->getCursorControl()->setVisible(false);

	m_Receiver = new EventReceiver(m_Device);
	m_Device->setEventReceiver(m_Receiver);

	return true;
}

bool Game::InitObjects()
{
	/*m_AstronautMesh = m_SceneManager->getMesh("../Assets/Models/astronaut_weapon.obj");
	if (!m_AstronautMesh)
	{
		m_Device->drop();
		DestroyObjects();
		return false;
	}
	m_AstronautSceneNode = m_SceneManager->addMeshSceneNode(m_AstronautMesh);
	if (m_AstronautSceneNode)
	{
		m_AstronautSceneNode->setMaterialFlag(EMF_LIGHTING, false);
	}*/

	m_Astronaut = m_ActorFactory.CreateActor("Actors/Astronaut.json", INVALID_ACTOR_ID);

	return true;
}

bool Game::DestroyObjects()
{
	m_SceneManager->clear();
	m_SceneManager->saveScene("Scenes/scene0.irr");
	m_Device->drop();
	return true;
}

void Game::RunLoop()
{
	int LastFPS = -1;
	u32 LastTime = m_Device->getTimer()->getTime();
	while (m_Device->run())
	{
		const u32 Now = m_Device->getTimer()->getTime();
        const f32 DeltaTime = (f32)(Now - LastTime) / 1000.0f;
        LastTime = Now;

        if (m_Device->isWindowActive())
        {
            int FPS = m_Driver->getFPS();
            if (FPS != LastFPS)
            {
                stringw str = _T("Sunfall: [");
                str += m_Driver->getName();
                str += "]: FPS: ";
                str += FPS;

                m_Device->setWindowCaption(str.c_str());

                LastFPS = FPS;
            }

            Update(DeltaTime);
            Render(DeltaTime);
        }
        else
        {
            m_Device->yield();
        }
		if (m_State == ShuttingDown) break;
	}
}

void Game::Update(f32 DeltaTime)
{
	if (m_Receiver->IsKeyDown(KEY_ESCAPE))
		m_State = ShuttingDown;

	/*vector3df NodePosition = m_AstronautSceneNode->getPosition();
	if (m_Receiver->IsKeyDown(KEY_KEY_W))
		NodePosition.Y += m_MovementSpeed * DeltaTime;
	if (m_Receiver->IsKeyDown(KEY_KEY_S))
		NodePosition.Y -= m_MovementSpeed * DeltaTime;
	if (m_Receiver->IsKeyDown(KEY_KEY_A))
		NodePosition.X -= m_MovementSpeed * DeltaTime;
	if (m_Receiver->IsKeyDown(KEY_KEY_D))
		NodePosition.X += m_MovementSpeed * DeltaTime;
	m_AstronautSceneNode->setPosition(NodePosition);*/

	m_Astronaut->Update(DeltaTime);
}

void Game::Render(f32 DeltaTime)
{
	m_Driver->beginScene(true, true, SColor(255, 100, 101, 140));

	m_SceneManager->drawAll();
	m_GUIEnv->drawAll();

	m_Driver->endScene();
}