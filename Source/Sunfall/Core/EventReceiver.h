#pragma once

class EventReceiver : public IEventReceiver
{
public:
	EventReceiver(IrrlichtDevice* Device);

	virtual bool OnEvent(const SEvent& Event);
	virtual bool IsKeyDown(EKEY_CODE KeyCode) const;

private:
	IrrlichtDevice* m_Device;
	bool m_KeysDown[KEY_KEY_CODES_COUNT];
};