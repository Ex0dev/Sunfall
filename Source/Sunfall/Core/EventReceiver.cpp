#include "stdafx.h"
#include "EventReceiver.h"

EventReceiver::EventReceiver(IrrlichtDevice* Device)
	: m_Device(Device)
{
	for (u32 i = 0; i < KEY_KEY_CODES_COUNT; i++)
	{
		m_KeysDown[i] = false;
	}
}

bool EventReceiver::OnEvent(const SEvent& Event)
{
	if (Event.EventType == EET_KEY_INPUT_EVENT)
		m_KeysDown[Event.KeyInput.Key] = Event.KeyInput.PressedDown;
	if (Event.EventType == EET_GUI_EVENT)
	{
		s32 Id = Event.GUIEvent.Caller->getID();
		IGUIEnvironment* Env = m_Device->getGUIEnvironment();

		switch (Event.GUIEvent.EventType)
		{
		default:
			break;
		}
	}

	return false;
}

bool EventReceiver::IsKeyDown(EKEY_CODE KeyCode) const
{
	return m_KeysDown[KeyCode];
}